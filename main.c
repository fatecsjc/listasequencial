#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

#define MAX 4

struct aluno {
    int matricula;
    char nome[30];
    float n1, n2;
};

struct lista{
    int qtd;
    struct aluno dados[MAX];
};

typedef struct lista Lista;

Lista *criaLista(){

    Lista *li;
    li = (Lista*) malloc(sizeof(struct lista));

    if(li != NULL)
        li->qtd = 0;

    return li;
}

int tamanhoLista(Lista *li){

    // ou return li == NULL ? -1 : (*li).qtd;
    return li == NULL ? -1 : li->qtd;
}

int listaCheia(Lista *li){

    return (li == NULL) ? -1 : li->qtd == MAX;
}

int listaVazia(Lista *li){

    return li == NULL ? -1 : li->qtd == 0;
}

int inserirNoFinalDaLista(Lista *li, struct aluno al){

    if(li == NULL || listaCheia(li))
        return 0;

    li->dados[li->qtd] = al;
    li->qtd++;

    return 1;
}

int inserirNoInicioDaLista(Lista *li, struct aluno al){

    if(li == NULL || listaCheia(li))
        return 0;

    for(int i = li->qtd - 1; i >= 0; i--)
        li->dados[i + 1] = li->dados[i];

    li->dados[0] = al;
    li->qtd++;

    return 1;
}

void exibirInformacoesDaLista(li){

    printf("\nTamanho lista: %d", tamanhoLista((Lista *) li));
    printf("\nLista cheia: %d", listaCheia((Lista *) li));
    printf("\nLista vazia: %d", listaVazia((Lista *) li));
    printf("\n");
}

void imprimirAlunos(Lista *li){

    for(int i = 0; i < li->qtd; i++){
        printf("Matrícula: %d", li->dados[i].matricula);
        printf("\tNome: %s", li->dados[i].nome);
        printf("\tNota 1: %2.2f", li->dados[i].n1);
        printf("\tNota 2: %2.2f", li->dados[i].n2);
        printf("\n");
    }
}


int main()
{
    setlocale(LC_ALL, "Portuguese");

    Lista *li;
    li = criaLista();

    exibirInformacoesDaLista((int) li);

    struct aluno a1;
    a1.matricula = 1;
    strcpy(a1.nome, "Robson");
    a1.n1 = 7.5f;
    a1.n2 = 8.5f;

    int x = inserirNoFinalDaLista(li, a1);
    printf("\n%d", x);

    exibirInformacoesDaLista((int) li);

    struct aluno a2;
    a2.matricula = 2;
    strcpy(a2.nome, "Logan");
    a2.n1 = 7.0f;
    a2.n2 = 8.0f;

    x = inserirNoFinalDaLista(li, a2);
    printf("\n%d", x);

    exibirInformacoesDaLista((int) li);

    struct aluno a3;
    a3.matricula = 3;
    strcpy(a3.nome, "Isabella");
    a3.n1 = 9.0f;
    a3.n2 = 8.6f;

    x = inserirNoInicioDaLista(li, a3);
    printf("\n%d", x);

    exibirInformacoesDaLista((int) li);

    imprimirAlunos(li);


    return 0;
}
